package com.aiconoa.trainings.tests.transaction;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.aiconoa.trainings.tests.entity.Account;

@RunWith(Arquillian.class)
public class TransactionServiceTest {

	@Inject
	private TransactionService transactionService;

	@PersistenceContext(unitName = "bank")
	private EntityManager em;

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class)
				.addClasses(Account.class, TransactionService.class, AccountNotFoundException.class, DepositException.class, WithdrawException.class)
				.addAsResource("META-INF/persistence-test.xml", "META-INF/persistence.xml");
	}

	@Test
	public void transactionServiceExists() {
		assertNotNull(transactionService);
	}

	@Test
	@UsingDataSet("datasets/accounts.yml")
	@ShouldMatchDataSet("datasets/expected-accounts.yml")
	public void depositMoneyIncreasesAccountBalance() {
		transactionService.deposit("test-account-number-1", new BigDecimal("11.35"));
	}
	
	@Test
	@UsingDataSet("datasets/accounts.yml")
	public void depositMoneyOnNonExistingAccountThrowsDepositException() {
		exception.expect(DepositException.class);
		transactionService.deposit("test-account-number-fake", new BigDecimal("4.67"));
	}

	@Test
	@UsingDataSet("datasets/withdraw-accounts.yml")
	@ShouldMatchDataSet("datasets/expected-withdraw-accounts.yml")
	public void withdrawMoneyDecreasesAccountBalance() {
		transactionService.withdraw("test-account-number-widthdraw-1", new BigDecimal("8.42"));
	}
	
	@Test
	@UsingDataSet("datasets/withdraw-accounts.yml")
	public void withdrawMoneyNegativeBalanceThrowsWidthdrawException() {
		exception.expect(WithdrawException.class);
		transactionService.withdraw("test-account-number-widthdraw-1", new BigDecimal("40"));
	}
	
	@Test
	@UsingDataSet("datasets/accounts.yml")
	public void withdrawMoneyOnNonExistingAccountThrowsWithdrawException() {
		exception.expect(WithdrawException.class);
		transactionService.withdraw("test-account-number-fake", new BigDecimal("4.67"));
	}

	// @Test(expected=DepositException.class)
	// public void depositMoneyOnUnexistingAccountThrowsDepositException() {
	// Account accountBeforeDeposit = createAccount("fake-account-number", new
	// BigDecimal(0));
	// transactionService.deposit(accountBeforeDeposit, new BigDecimal("8.54"));
	// }
	//
	// @Test(expected=DepositException.class)
	// public void depositNegativeAmountThrowsDepositException() {
	// Account accountBeforeDeposit = createAccount("test-account-number-1", new
	// BigDecimal(0));
	// transactionService.deposit(accountBeforeDeposit, new
	// BigDecimal("-3.43"));
	// }

	// @Test
	// public void depositMoneyIncreasesAccountBalance() {
	//
	// // ENTITY
	// Account accountBeforeDeposit = new Account(accountNumber);
	// double amount = 11.35;
	//// Account accountAfterDeposit = new Account(initialBalance + 11.35);
	//
	// // EJB
	// TransactionService transactionService = new TransactionService(); // pas
	// de new, injection requise !
	//
	// Account accountAfterDeposit =
	// transactionService.deposit(accountBeforeDeposit, amount);
	//
	// assertEquals(accountAfterDeposit.getBalance() - amount,
	// accountBeforeDeposit.getBalance());
	// }

	// TODO: quid si on tente de faire un deposit <= 0 ?
	// TODO: règles métier: trop gros dépot interdits ? etc...
	// TODO: deux dépots sur le même compte, voir si les transactions ne se
	// mélangent pas les pinceaux

}
