package com.aiconoa.trainings.tests.transaction;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class DepositException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DepositException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
