package com.aiconoa.trainings.tests.transaction;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class WithdrawException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public WithdrawException(String message, Throwable cause) {
		super(message, cause);
	}

	public WithdrawException(String message) {
		super(message);
	}

}
