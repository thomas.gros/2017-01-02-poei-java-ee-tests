package com.aiconoa.trainings.tests.transaction;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.aiconoa.trainings.tests.entity.Account;

@Stateless
public class TransactionService {

	@PersistenceContext(unitName="bank")
	private EntityManager em;
	
	public void deposit(String account, BigDecimal amount) {
		
		Account accountBeforeDeposit;
		try {
			accountBeforeDeposit = findAccountByAccountNumber(account);
		} catch (AccountNotFoundException ex) {
			throw new DepositException("Tried to make a deposit on an unknown account number " + account, ex);
		}

		em.merge(new Account(accountBeforeDeposit.getId(),
							 accountBeforeDeposit.getNumber(), 
							 accountBeforeDeposit.getBalance().add(amount),
							 accountBeforeDeposit.getVersion()));
		
	}

	public void withdraw(String account, BigDecimal amount) {
		Account accountBeforeWithdraw;
		try {
			accountBeforeWithdraw = findAccountByAccountNumber(account);
		} catch (AccountNotFoundException ex) {
			throw new WithdrawException("Tried to make a withdraw on an unknown account number " + account, ex);
		}
		
		if(accountBeforeWithdraw.getBalance().subtract(amount).signum() < 0) {
			throw new WithdrawException("Account cannot have a negative balance");
		}
		
		em.merge(new Account(accountBeforeWithdraw.getId(),
							accountBeforeWithdraw.getNumber(), 
							accountBeforeWithdraw.getBalance().subtract(amount),
				 			accountBeforeWithdraw.getVersion()));
	}
	
	private Account findAccountByAccountNumber(String accountNumber) {
		try {
			return  em.createQuery("From Account a WHERE a.number = :accountNumber", Account.class)
					  .setParameter("accountNumber", accountNumber)
					  .getSingleResult();
		} catch (NoResultException ex) {
			throw new AccountNotFoundException("account " + accountNumber + " does not exist");
		}
	}

}
