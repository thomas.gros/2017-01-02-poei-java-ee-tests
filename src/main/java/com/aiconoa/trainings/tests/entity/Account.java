package com.aiconoa.trainings.tests.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="account")
public class Account {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String number;
	private BigDecimal balance;
	
	@Version
	private Long version;
	
	public Account() {}
	
	public Account(Long id, String number, BigDecimal balance, Long version) {
		this.id = id;
		this.number = number;
		this.balance = balance;
		this.version = version;
	}

	public Long getId() {
		return id;
	}
	
	public String getNumber() {
		return number;
	}
	
	public BigDecimal getBalance() {
		return balance;
	}
	
	public Long getVersion() {
		return version;
	}
}
